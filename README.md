# Zoidberg AI Xray Chest analysis
### Run the project
1. Run with CPU
- clone repo
- cd zoidberg2.0/notebooks && clone dataset && mv dataset/chest_Xray . && cd ../..
- `docker run -v /app1:zoidberg2.0 -p 8888:8888 --name nb1 jupyter`
- `jupyter notebook`

2. Run with GPU
- clone repo
- docker run -v /app1:zoidberg2.0 -p 8888:8888 --name nb1 jupyter

### Nvidia monitoring 
```
Sun May  9 08:49:41 2021       
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 460.73.01    Driver Version: 460.73.01    CUDA Version: 11.2     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|                               |                      |               MIG M. |
|===============================+======================+======================|
|   0  GeForce GTX 108...  Off  | 00000000:08:00.0 Off |                  N/A |
|  0%   40C    P2    74W / 250W |   9176MiB / 11178MiB |     98%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+
                                                                               
+-----------------------------------------------------------------------------+
| Processes:                                                                  |
|  GPU   GI   CI        PID   Type   Process name                  GPU Memory |
|        ID   ID                                                   Usage      |
|=============================================================================|
|    0   N/A  N/A      1395      G   /usr/lib/xorg/Xorg                133MiB |
|    0   N/A  N/A      3217      G   /proc/self/exe                     67MiB |
|    0   N/A  N/A     16014      C   /opt/conda/bin/python            8837MiB |
+-----------------------------------------------------------------------------+
```